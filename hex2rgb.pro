TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

MODE = ""

CONFIG(debug, debug|release) {
    MODE = "Debug"
} else {
    MODE = "Release"
}


SOURCES += \
    $$PWD/src/main.cpp

INCLUDEPATH = \
    $$PWD/src/headers

OBJECTS_DIR = \
    $$PWD/obj/$$MODE/src
