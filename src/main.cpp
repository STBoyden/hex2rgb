#include <boost/lexical_cast.hpp>
#include <iostream>
#include <math.h>
#include <regex>
#include <string>
#include <vector>

int main(int argc, char* argv[])
{
  std::string str;
  unsigned int beginPoint;

  if (argc != 2 || argc > 2) {
    std::cerr << "Usage: hex2rgb <string>" << std::endl;
    std::cout << "(HINT: Try escaping '#' like '\\#')" << std::endl;
    exit(1);
  }

  str.insert(0, argv[1]);

  std::vector<std::string> array;

  beginPoint = str[0] == '#' ? 1 : 0;

  if (((str.size() <= 5) || str.size() > 7)
      || (str.size() == 7 && beginPoint == 0)
      || (str.size() == 6 && beginPoint == 1)) {
    std::cerr << "Input HEX string should be either 6 (without '#') or 7 (with "
                 "\\#) characters long"
              << std::endl;
    exit(1);
  }

  for (unsigned int i = beginPoint; i < str.size(); i += 2) {
    int val1, val2;

    if (std::regex_search(str, std::regex("[^#A-Fa-f0-9]"))) {
      std::cerr << "Error: invalid HEX character" << std::endl;
      std::cout << "(HINT: HEX characters are 0-9 and A-F)" << std::endl;
      exit(1);
    }

    switch (str[i]) {
    case 'a':
    case 'A':
      val1 = 10;
      break;
    case 'b':
    case 'B':
      val1 = 11;
      break;
    case 'c':
    case 'C':
      val1 = 12;
      break;
    case 'd':
    case 'D':
      val1 = 13;
      break;
    case 'e':
    case 'E':
      val1 = 14;
      break;
    case 'f':
    case 'F':
      val1 = 15;
      break;
    default:
      val1 = boost::lexical_cast<int>(std::string(1, str[i]));
      break;
    }

    switch (str[i + 1]) {
    case 'a':
    case 'A':
      val2 = 10;
      break;
    case 'b':
    case 'B':
      val2 = 11;
      break;
    case 'c':
    case 'C':
      val2 = 12;
      break;
    case 'd':
    case 'D':
      val2 = 13;
      break;
    case 'e':
    case 'E':
      val2 = 14;
      break;
    case 'f':
    case 'F':
      val2 = 15;
      break;
    default:
      val2 = boost::lexical_cast<int>(std::string(1, str[i + 1]));
      break;
    }

    val1 = (val1 * std::pow(16, 1));
    val2 = (val2 * std::pow(16, 0));

    auto chars = std::to_string(val1 + val2);

    array.push_back(chars);
  }

  std::string hexStringToPrint;

  if (str[0] == '#')
    hexStringToPrint = str;
  else
    hexStringToPrint = std::string("#") + str;

  std::cout << "Original HEX string: " << std::string(hexStringToPrint)
            << std::endl;
  std::cout << std::string("r: ") + array[0] + std::string(" g: ") + array[1]
          + std::string(" b: ") + array[2]
            << std::endl; // r: x g: y b: z
  std::cout << std::string("rgb(") + array[0] + std::string(",") + array[1]
          + std::string(",") + array[2] + std::string(")")
            << std::endl; // rgb(x,y,z)
  return 0;
}
